import pyautogui
import logging
import datetime
from time import sleep
import win32clipboard
import pandas as pd
import configparser
import sys, os
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
import win32com.client




class C3_SWAP:

    def __init__(self):

        # create logger
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)

        # create console handler
        ch = logging.StreamHandler()

        #create file handler 
        date = str(datetime.datetime.now().strftime('%d-%b-%Y %H_%M_%S %p'))
        fh = logging.FileHandler('debug\\{}.log'.format(date),encoding='utf-8')

        # create formatter
        formatter = logging.Formatter('%(asctime)s - %(funcName)s - %(levelname)s - %(message)s',datefmt='%d/%b/%Y %I:%M:%S %p')

        # add formatter to ch
        ch.setFormatter(formatter)

        #add formatter to fh
        fh.setFormatter(formatter)

        # add ch to logger
        self.logger.addHandler(ch)

        #add fh to logger
        self.logger.addHandler(fh)


        #config.init file
        self.my_config_parser = configparser.ConfigParser()

        self.my_config_parser.read('config\\config.ini')

        self.config = { 

        'email': self.my_config_parser.get('C3_LOGIN','email'),
        'password': self.my_config_parser.get('C3_LOGIN','password'),
        'prog_path': self.my_config_parser.get('C3_LOGIN','prog_path'),
        'TO': self.my_config_parser.get('C3_LOGIN','TO'),
        'CC': self.my_config_parser.get('C3_LOGIN','CC'),
        'attach_path': self.my_config_parser.get('C3_LOGIN','attach_path')
        

        }

        self.err_msg = {}

        pyautogui.PAUSE = 0.1

        self.en = pyautogui.password(text='Please Enter Your \"EN\"', title='Step 1 : EN', mask='☠')

        self.logger.info('EN : '+self.en)

        if self.en in  ['509357','316339','027958','27958','514939']:

            self.pass_code = pyautogui.prompt(text='Please Enter \"Pass Code\"', title='Step 2 : Pass Code')

        else:

            pyautogui.alert(text='Incorrect EN !!!',title='Error!',button='Exit')

            sys.exit()


        # clear old file

        if os.path.exists(self.config['prog_path']):

            os.remove(self.config['prog_path'])

            self.logger.debug('Already Removed old file >>> '+self.config['prog_path'])

        else:

            self.logger.debug('Not found old file')


    
    def login(self):

        self.driver=webdriver.Chrome()

        self.driver.get("https://wwwin-csfprd.cisco.com")

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@id="userInput"]'))).send_keys(self.config["email"])

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="login-button"]'))).click()

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@id="passwordInput"]'))).send_keys(self.config["password"])

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@id="login-button"]'))).click()

        count_render_2fa = 0

        while (self.driver.title != "Two-Factor Authentication"):

            sleep(1)

            count_render_2fa+=1

            self.logger.info("Wait for two-factor authentication render:"+str(count_render_2fa))


        push_login_btn = WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//button[@type="submit"]')))

        push_login_btn_text = push_login_btn.text

        if push_login_btn_text == "Send Me a Push":

            clickable = False

            while(not clickable):

                try:

                    WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//button[@id="passcode"]'))).click()

                    clickable = True

                    WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="passcode"]'))).send_keys(self.pass_code)

                    WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//button[@id="passcode"]'))).click()

                except:

                    self.logger.warning("Can't Clickable'")
                
        else:
            self.logger.info("Not found \"Send Me a Push\" button")


        two_fa_url=self.driver.current_url

        count_duo_pass = 0

        while(two_fa_url==self.driver.current_url):

            sleep(1)

            count_duo_pass+=1

            self.logger.info("Wait for count_duo_pass:"+str(count_duo_pass))

            if count_duo_pass == 30:

                self.logger.warning("!!! LOGIN TIMEOUT !!!")

                self.driver.quit()

                sys.exit()

        self.logger.info("Login to C3 is success!!!")

        sleep(1)

        WebDriverWait(self.driver, 10).until(ec.visibility_of_all_elements_located((By.XPATH, '//a[@id="AppsNavLink"]')))[1].click()

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//a[@id="N352"]'))).click()

        while True:

            if os.path.exists(self.config['prog_path']):

                self.logger.debug('Already Downloaded Java C3')

                break

            else:

                self.logger.debug('Wait for download Java C3')

                sleep(1)

        os.system(self.config['prog_path'])

        
        return True
    



    def readExcel(self):

        self.df = pd.read_excel('C3_Swap_Template.xlsm')

        self.logger.debug(self.df)

        self.df_total = self.df.groupby(['PID','FROM','TO','REFERENCE'],as_index=False)['QUANTITY'].sum()

        self.df_total['FROM'] = pd.Categorical(self.df_total['FROM'],['FA-REC','FA-WIP','FA-HOLD','FA-LAB'])

        self.df_total_sort = self.df_total.sort_values(['FROM'])

        self.logger.debug(self.df_total_sort)

        self.df_total_sort = self.df_total_sort.reset_index(drop=True)

        self.logger.debug(self.df_total_sort)

        #column defined 
        self._pid = self.df_total_sort['PID']
        self._from = self.df_total_sort['FROM']
        self._to= self.df_total_sort['TO']
        self._quantity= self.df_total_sort['QUANTITY']
        self._reference = self.df_total_sort['REFERENCE']

        return True


    def findObject(self,path):

        while True:

            try:
                self.foundObject = pyautogui.locateOnScreen(path)
                
                self.logger.debug(self.foundObject)

                if self.foundObject == None:

                    self.logger.warning('Not found Object : {}'.format(path))

                    sleep(1)

                else :

                    self.logger.info('Found Object : {}'.format(path))

                    self.x, self.y, self.w, self.h = self.foundObject

                    break

            except:

                self.logger.error('Error Finding Object : {}'.format(path))


    def perform(self):

        self.findObject('src\\th_fabrinet.png')

        pyautogui.click(x=self.x+self.w/1.5,y=self.y+self.h/2)

        self.findObject('src\\ok.png')

        pyautogui.click(x=self.x+self.w/1.5,y=self.y+self.h/2)

        self.findObject('src\\type.png')

        pyautogui.click(x=self.x+self.w/1.5,y=self.y+self.h/2)      

        pyautogui.typewrite("s")

        pyautogui.press('tab')

        self.findObject('src\\transaction_line.png')

        pyautogui.click(x=self.x+self.w/1.5,y=self.y+self.h/2)

        self.remain = {}

        for i in range(self.df_total_sort.index.__len__()):

            self.logger.info(self._pid[i] + ' >>> FROM : ' + self._from[i] + ' >>> TO : ' + self._to[i] + '>>> REFERENCE : '+self._reference[i] + ' >>> QUANTITY : ' + str(self._quantity[i]))

            self.findObject('src\\item.png')
            
            pyautogui.typewrite(self._pid[i])

            pyautogui.press('tab')

            pyautogui.typewrite(self._from[i])

            #check available units

            pyautogui.press('tab')

            self.findObject('src\\to_sub_in.png')

            self.findObject('src\\available_unit.png')

            pyautogui.doubleClick(x=self.x+self.w*1.5,y=self.y+self.h/2)     

            pyautogui.keyDown('ctrl')

            pyautogui.press('c')

            pyautogui.keyUp('ctrl')

            win32clipboard.OpenClipboard(0)

            try:

                self.check_sn = int(win32clipboard.GetClipboardData())

                if self.check_sn == 0:

                    self.remain[i] = '0'

                    msg = self._pid[i] + ' >>> FROM : ' + self._from[i] + ' >>> TO : ' + self._to[i] + '>>> REFERENCE : '+self._reference[i] + ' >>> QUANTITY : ' + str(self._quantity[i])  + ' >>> ERROR :  Available Unit is Null !!!' 

                    self.logger.critical(msg)

                    self.err_msg[i] = msg

                    self.findObject('src\\to_sub.png')

                    pyautogui.click(x=self.x+self.w/1.5,y=self.y+self.h/2)    

                    pyautogui.keyDown('shift')

                    pyautogui.press('tab',2)

                    pyautogui.keyUp('shift')

                    continue

            except:
                
                self.remain[i] = str(self._quantity[i])
                
                msg = self._pid[i] + ' >>> FROM : ' + self._from[i] + ' >>> TO : ' + self._to[i] + '>>> REFERENCE : '+self._reference[i] + ' >>> QUANTITY : ' + str(self._quantity[i])  + ' >>> ERROR :  Available Unit is Negative !!!' 

                self.logger.critical(msg)

                self.err_msg[i] = msg

                self.findObject('src\\to_sub.png')

                pyautogui.click(x=self.x+self.w/1.5,y=self.y+self.h/2)    

                pyautogui.keyDown('shift')

                pyautogui.press('tab',2)

                pyautogui.keyUp('shift')

                continue

            finally:

                win32clipboard.CloseClipboard()


            self.findObject('src\\to_sub.png')

            pyautogui.click(x=self.x+self.w/1.5,y=self.y+self.h/2)   

            pyautogui.typewrite(self._to[i])

            pyautogui.press('tab',2)

            self.findObject('src\\quantity.png')

            if self.check_sn >= self._quantity[i] :

                pyautogui.typewrite(str(self._quantity[i]))

            else:

                self.remain[i] = str(self._quantity[i]-self.check_sn)

                msg = self._pid[i] + ' >>> FROM : ' + self._from[i] + ' >>> TO : ' + self._to[i] + '>>> REFERENCE : '+self._reference[i] + ' >>> QUANTITY : ' + str(self._quantity[i])  + ' >>> ERROR :  Available Units not enough !!!' + ' >>> REMAIN : '+str(self._quantity[i]) + ' - ' +str(self.check_sn) + ' = ' +str(self._quantity[i]-self.check_sn) + ' Units'

                self.logger.critical(msg)

                self.err_msg[i] = msg

                pyautogui.typewrite(str(self.check_sn))


            pyautogui.press('tab',2)

            self.findObject('src\\reference.png')

            pyautogui.typewrite(self._reference[i])

            pyautogui.press('down')



    def main(self):

        self.logger.info('*** Start...  >>>  login() ***')

        self.login()

        self.logger.info('*** Start...  >>>  readExcel() ***')

        self.readExcel()

        self.logger.info('*** Start...  >>>  perform() ***')

        self.perform()

        self.logger.info('*** Finish...  >>>  Result ***')

        self.logger.critical(self.err_msg)

        self.log = datetime.datetime.now()

        self.log = '\n'+'\n'+str(self.log)+ ' >>> EN: '+ self.en +'\n'+'\n'

        for i,j in self.err_msg.items():

            self.log+=str(i)+': '+ j + '\n'+ '\n'

        with open('result.txt', 'a') as file:

            file.write(self.log)



if __name__ == '__main__':

    run = C3_SWAP()

    run.main()

    if len(run.err_msg) > 0:

        result = run.log
    
    else:

        result = 'Complete All FA Cases !!!'

    while True:

        try:

            en = pyautogui.password(text=result, title='Result', mask='☠') 

            if en in ['509357','316339','027958','27958','514939']:

                if result != 'Complete All FA Cases !!!':

                    outlook = win32com.client.Dispatch('outlook.application')

                    mail = outlook.CreateItem(0)

                    mail.To = run.config['TO']

                    mail.Subject = 'C3 Swap Issues !!!'

                row_table = ''

                for i in run.err_msg:

                    row_table +=f'<tr> <td>{run._pid[i]}</td> <td>{run._from[i]}</td><td>{run._to[i]}</td><td>{run._reference[i]} <td>{run._quantity[i]}</td> <td>{run.remain[i]}</td></tr>'

                print(row_table)

                mail.HTMLBody = f"""

                <table>
                  <tr>
                    <th>PID</th>
                    <th>FROM</th>
                    <th>TO</th>
                    <th>REFERENCE</th>
                    <th>QUANTITY</th>
                    <th>REMAINING</th>
                  </tr>
                    {row_table}
                </table>

                """

                # mail.Body = result

                mail.CC = run.config['CC']

                mail.Attachments.Add(run.config['attach_path'])

                mail.Send()

                break

            run.logger.critical('Incorrect EN >>> '+en)

        except:

            run.logger.critical('Incorrect EN >>> Error Result Box !')
    
    run.logger.info('End of program >>> '+en)

    run.driver.quit()

